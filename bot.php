<?php
require "src/MyTelegramSender.php";

use Src\MyTelegramSender;


$tg = new MyTelegramSender;
if (strpos($tg->command, '/start') !== faLse) {
    if ($tg->command == '/start') {
        $tg->db->query('SELECT * FROM `users` WHERE `user_id` = :user_id');
        $tg->db->bindValue('user_id', $tg->userId);
        $res = $tg->db->execute();
        $tg->addContentsToFile('errors.txt', $res);
        $count = $tg->db->count();
        if ($count == 0) {
            $tg->setText(['text' => 'Hello']);
            $tg->sendCurlRequest();
            $now = date('Y-m-d H:i:s');
            $tg->db->query("INSERT INTO `users` (`user_id` ,`first_name`,`last_name`, `username` ,`created_at` ,`step`) VALUES (:user_id,:first_name ,:last_name ,:user_name ,:created_at , :step)");
            $tg->db->bindValue('user_id', $tg->userId);
            $tg->db->bindValue('first_name', $tg->firstName);
            $tg->db->bindValue('last_name', $tg->lastName);
            $tg->db->bindValue('user_name', $tg->userName);
            $tg->db->bindValue('created_at', $now);
            $tg->db->bindValue('step', 'home');
            $tg->addContentsToFile('errors.txt', print_r($tg, 1));
            $res = $tg->db->execute();
        }
    } else {
        $referralId = substr($tg->command, 7);
        if ($referralId == $tg->userId) {
            $tg->setText(['text' => 'Not now bro']);
            $tg->sendCurlRequest();
            die;
        }
        $tg->db->query('SELECT * FROM `user_invite` where `user_id` = :user_id');
        $tg->db->bindValue('user_id', $tg->userId);
        $tg->db->execute();
        if ($tg->db->count() == 1) {
            $tg->setText(['text' => 'این یوزر از قبل در ربات عضو شده بوده است', 'user_id' => $referralId]);
            $tg->sendCurlRequest();

        }

        $tg->db->query("INSERT INTO `user_invite` (`inviter_id` , `user_id`) values  (:inviter_id,:user_id) ");
        $tg->db->bindValue('inviter_id', $referralId);
        $tg->db->bindValue('user_id', $tg->userId);
        $res = $tg->db->execute();
        if ($res) {
            $tg->setText(['text' => 'New user invited', 'user_id' => $referralId]);
            $tg->sendCurlRequest();
        }
    }
    $tg->setText(['text' => 'Hello User! How are u?', 'buttons' => [
        'keyboard' => [
            [
                ['text' => 'Phone number', 'request_contact' => true],
                ['text' => 'Location', 'request_location' => true],
                ['text' => 'Get Chat now!'],
                ['text' => 'Is user exists']
            ],
            [
                ['text' => 'Get your invite link'],
                ['text' => 'How many i invited?'],
            ],
            [
                ['text' => 'Locked section'],
            ]
        ]
        , 'resize_keyboard' => true]]);
    $tg->sendCurlRequest();

} elseif ($tg->userPhone != null) {
    $tg->db->query('UPDATE `users` SET phone = :phone WHERE user_id = :user_id');
    $tg->db->bindValue('phone', $tg->userPhone);
    $tg->db->bindValue('user_id', $tg->userId);
    $res = $tg->db->execute();
    if ($res) {
        $tg->setText(['text' => 'Here we are!']);
        $tg->sendCurlRequest();
    }
} elseif ($tg->userLongitude != null and $tg->userLatitude) {
    $tg->setText(['text' => 'Heloo']);
    $tg->sendCurlRequest();
    $tg->db->query('UPDATE `users` SET `longitude` = :longitude , `latitude` = :latitude where user_id = :user_id');
    $tg->db->bindValue('longitude', $tg->userLongitude);
    $tg->db->bindValue('latitude', $tg->userLatitude);
    $tg->db->bindValue('user_id', $tg->userId);
    $res = $tg->db->execute();
    $tg->addContentsToFile('errors.txt', $res);
    if ($res) {
        $tg->setText(['text' => 'Yeea You Got it!']);
        $tg->sendCurlRequest();
    }
} elseif ($tg->command == 'Get Chat now!') {
    $tg->setText(['text' => 'Yea we got chat']);
    $tg->sendCurlRequest();
    $tg->getChat();
    $data = $tg->sendCurlRequest();
    $tg->addContentsToFile('errors.txt', print_r($tg->getUserContents($data, 1, 1), 1));
} elseif ($tg->command == 'Is user exists') {
    $tg->getChatMember(['user_id' => $tg->userId, 'chat_id' => '@MatinwdTEst']);
    $res = $tg->sendCurlRequest();
    $data = $tg->getUserContents($res, 1, 1);
    $userStatus = $data['result']['status'];
    $tg->addContentsToFile('errors.txt', $userStatus);
    if (in_array($userStatus, $tg->userStatus)) {
        $tg->setText(['text' => 'User Exists!']);
        $tg->sendCurlRequest();
    } else {
        $tg->setText(['text' => 'You should join our channel']);
        $tg->sendCurlRequest();
    }
} elseif ($tg->command == 'Get your invite link') {
    $text = ("Hello \n  matin \n Here is your invite link https://t.me/MatinTest1Bot?start=" . $tg->userId);
    $tg->setText(['text' => $text]);
    $tg->sendCurlRequest();
}
elseif ($tg->command == 'How many i invited?') {
    $tg->db->query('SELECT * FROM `users` u JOIN `user_invite` ui ON u.user_id = ui.inviter_id where ui.inviter_id = :user_id');
    $tg->db->bindValue('user_id' , $tg->userId);
    $tg->db->execute();
    $res = $tg->db->resultAll();
    $count = $tg->db->count();
    $tg->setText(['text' => "U invited {$count}"]);
    $tg->sendCurlRequest();
}
elseif ($tg->command == 'Locked section') {
    $tg->db->query('SELECT * FROM `users` u JOIN `user_invite` ui ON u.user_id = ui.inviter_id AND ui.inviter_id = :user_id');
    $tg->db->bindValue('user_id' , $tg->userId);
    $tg->db->execute();
    $res = $tg->db->resultAll();
    $count = $tg->db->count();
    $shouldInvite = 2 - $count;
    if($count > 2){
        $tg->setText(['text' => date('Y-m-d H:i:s')]);
        $tg->sendCurlRequest();
    }else{
        $tg->setText(['text' => "u should invite {$shouldInvite}"]);
        $tg->sendCurlRequest();
    }
}
else {
    $tg->setText(['text' => 'Command not found']);
    $tg->sendCurlRequest();
}