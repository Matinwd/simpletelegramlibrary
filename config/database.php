<?php

namespace Config;
use PDO;

class Database
{

    private $stmt;
    private $pdo;
    private $username = __USERNAME__;
    private $password = __PASSWORD__;
    private $dbname = __DBNAME__;
    private $host = __HOST__;

    public function __construct()
    {
        $this->pdo = new PDO("mysql:host={$this->host};dbname={$this->dbname}", $this->username, $this->password, [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        ]);
    }

    public function query($query)
    {
        try {
            $this->stmt = $this->pdo->prepare($query);
        } catch (PDOException $exception) {
            echo $exception->getMessage();
        }
    }

    public function bindValue($param, $value)
    {
        $this->stmt->bindValue(':' . $param, $value);
    }

    public function execute()
    {
        return $this->stmt->execute();
    }

    public function resultAll()
    {
        return $this->stmt->fetchAll(PDO::FETCH_OBJ);
    }

    public function resultOne()
    {
        return $this->stmt->fetch(PDO::FETCH_OBJ);
    }

    public function count()
    {
        return $this->stmt->rowCount();
    }


}