A very simple telegram bot library for php.

First of all you have to make sure you copied your bot token and pasted in config.php

After that you should require bot.php in your project and 

* A simple way to use library. You should make an instance from project and use the following methods
 * You should set something and then, you can send a request.
 * All requests will send with curl ext.
 * All parameters will defined in $params
 * Hope you enjoy it :)
 
 
 
// Will set one photo link or file_id of a photo that stored in telegram servers (Uploaded in telegram)
/*$tg->setPhoto(['file_id' => 'https://tpc.googlesyndication.com/simgad/17566016470595348152?sqp=4sqPyQQrQikqJwhfEAEdAAC0QiABKAEwCTgDQPCTCUgAUAFYAWBfcAJ4AcUBLbKdPg&rs=AOga4qlh9DLJPmsJ-ep-sOO30o-hL72Qgw', 'caption' => 'haha']);
$tg->sendCurlRequest();
// Will set one video link or file_id of a video that stored in telegram servers (Uploaded in telegram)
$tg->setVideo(['file_id' => 'BAACAgQAAxkBAAIBQl5yJOtvE5MrTaGDGysPvMmVldtQAAKBBgACoKeRU_WvF2EOfJL0GAQ', 'caption' => 'ویدیو تست', 'disable_notification' => false, 'reply' => true, 'duration' => false, 'stream_support' => false]);
$tg->sendCurlRequest();

// Will set one Animation link or file_id of a animation that stored in telegram servers (Uploaded in telegram)
$tg->setAnimation(['file_id' => 'CgACAgIAAxkBAAIBUV5yJbg4C4sm5Fde48UFZmIalRkLAALvAgACu0sYSO4lTdLdSG08GAQ', 'caption' => 'ین یک انیمیشن هست', 'disable_notification' => false, 'reply' => true, 'duration' => false]);
$tg->sendCurlRequest();

// Will set one document link or file_id of a document that stored in telegram servers (Uploaded in telegram)
$tg->setDocument(['file_id' => 'BQACAgEAAxkBAAIEhV5zsF1ccz4Y8_AX90OR0WUCvU2VAAJrAQACCWMYR5lfnYgZV-PFGAQ', 'caption' => 'یک فایل تست']);
$tg->sendCurlRequest();

// Will set one audio link or file_id of a audio that stored in telegram servers (Uploaded in telegram)
$tg->setAudio(['file_id' => 'CQACAgQAAxkBAAIBeF5yKTPECLsMSRSjiYhR7ofXeiMgAAKABgAC9JyQU2b5eB6OhgrCGAQ', 'title' => 'Matin Wd', 'performer' => 'Haha', 'duration' => false]);
$tg->sendCurlRequest();

// Will set one voice link or file_id of a voice that stored in telegram servers (Uploaded in telegram)
$tg->setVoice(['file_id' => 'AwACAgQAAxkBAAIBtl5yK21eXAmO_HUsJYpVWC62WmWSAAJ_BgAC9JyQU0_Dkd1bCyhMGAQ', 'caption' => 'Ahahaha', 'duration' => false]);
$tg->sendCurlRequest();

// Will set one Video note file_id of a video note that stored in telegram servers (Uploaded in telegram)
$tg->setVideoNote(['file_id' => 'DQACAgQAAxkBAAIBzF5yLF7WsxlMKS0Q_nSNJQvv96rGAAL_BgACKw6IUxudYS5AskRgGAQ', 'caption' => 'Noooooo']);
$tg->sendCurlRequest();


// Will set a media group of photos or videos
$tg->setMediaGroup(['media' => [
    [
        'type' => 'photo',
        'media' => 'https://tpc.googlesyndication.com/simgad/17566016470595348152?sqp=4sqPyQQrQikqJwhfEAEdAAC0QiABKAEwCTgDQPCTCUgAUAFYAWBfcAJ4AcUBLbKdPg&rs=AOga4qlh9DLJPmsJ-ep-sOO30o-hL72Qgw'
    ],
    [
        'type' => 'video',
        'media' => 'BAACAgQAAxkBAAIBQl5yJOtvE5MrTaGDGysPvMmVldtQAAKBBgACoKeRU_WvF2EOfJL0GAQ'
    ],
    [
        'type' => 'video',
        'media' => 'BAACAgQAAxkBAAIBQl5yJOtvE5MrTaGDGysPvMmVldtQAAKBBgACoKeRU_WvF2EOfJL0GAQ'
    ]
], 'caption' => 'Hastma'
]);
$tg->sendCurlRequest();

// Will set a location
$tg->setLocation(['latitude' => 32.7067964, 'longitude' => 51.6592956, 'live_period' => null]);
$tg->sendCurlRequest();

// Will set a venue (A location with title and address)
$tg->setVenue(['latitude' => 32.7067964, 'longitude' => 51.6592956, 'title' => 'Matin', 'address' => 'Nooo matin']);
$tg->sendCurlRequest();

// Will set a contact
$tg->setContact(['phone' => '09335152312', 'first_name' => 'Maryam']);
$tg->sendCurlRequest();

// Will set a poll
$tg->setPoll(['question' => 'Hey how are you?', 'options' => ['na', 'are']]);
$tg->sendCurlRequest();

// Will set a chat action
$tg->setChatAction(['action' => 'typing']);
$tg->sendCurlRequest();

// will get all user profiles due to the Telegram API
$tg->getUserProfilePhotos(false);

// Will convert all profiles into an array. just file_id of them.
$photos = $tg->getArrayPhotos(
    json_decode($tg->sendCurlRequest(), 1)
);

// will send all photos of user for himself
foreach ($photos as $key => $photo) {
    $tg->setPhoto(['file_id' => $photo, 'caption' => 'این پروفایل شماست']);
    $tg->sendCurlRequest();
}

// will set an array for use in MediaPhotos
$mediaPhotos = $tg->setArrayPhotosGoodForMediaGroups($photos);

// Will set a media group of photos or videos
$tg->setMediaGroup([$mediaPhotos, 'These are your profiles']);
$err = $tg->sendCurlRequest();

// Will add some contents to some file. In below example, errors.txt.
$tg->addContentsToFile('errors.txt', $err, FILE_APPEND);

//$tg->setkickChatMember(['user_id' => null]); // if user id == null, it will kicks the current user that sent message
//$tg->sendCurlRequest();

// this will generate a new link for group
$tg->setexportChatInviteLink();
$linkRes = $tg->sendCurlRequest();
$newLink = json_decode($linkRes, 1);

$tg->setText(['text' => $newLink["result"]]);
$tg->sendCurlRequest();

$tg->setGroupPermissions([
    'permissions' => [
        'can_send_messages' => true,
    ]
]);
$errors = $tg->sendCurlRequest();
$tg->addContentsToFile('errors.txt', $errors);

/*$tg->setPinChatMessage();
$err = $tg->sendCurlRequest();*/

/*
$tg->deleteChatPhoto();
$tg->sendCurlRequest();*/


/*$tg->setUnPinChatMessage();
$errr = $tg->sendCurlRequest();
$tg->addContentsToFile('errors.txt', $errr);*/


/*$tg->setChatTitle(['title' => 'Test1']);
$tg->sendCurlRequest();

$tg->setChatDescription(['description' => 'MAatin am']);
$tg->sendCurlRequest();*/


// TODO
// There is some problems and i should fix them

/*$tg->setChatAdministratorCustomTitle(['user_id' => 705848932,'custom_title' => 'test 1']);
$errro = $tg->sendCurlRequest();
$tg->addContentsToFile('erros.txt', $errro);*/
/*
$tg->setChatPhoto(['file_id' => 'https://tpc.googlesyndication.com/simgad/17566016470595348152?sqp=4sqPyQQrQikqJwhfEAEdAAC0QiABKAEwCTgDQPCTCUgAUAFYAWBfcAJ4AcUBLbKdPg&rs=AOga4qlh9DLJPmsJ-ep-sOO30o-hL72Qgw']);
$err = $tg->sendCurlRequest();
$tg->addContentsToFile('errors.txt', $err);*/

